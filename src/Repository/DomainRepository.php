<?php

namespace App\Repository;

use App\Entity\Domain;
use App\Entity\City;
use App\Entity\Land;
use App\Entity\Post;
use App\Repository\CityRepository;
use App\Repository\LandRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Domain>
 *
 * @method Domain|null find($id, $lockMode = null, $lockVersion = null)
 * @method Domain|null findOneBy(array $criteria, array $orderBy = null)
 * @method Domain[]    findAll()
 * @method Domain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DomainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Domain::class);
    }

    public function save(Domain $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Domain $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllOptimized()
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.city', 'city')
            ->leftJoin('d.land', 'land')
            ->addSelect('city')
            ->addSelect('land')
            ->orderBy('d.updatedAt', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findByRandom(int $limit = 4): array
    {
        return $this->createQueryBuilder('d')
            ->leftJoin('d.city', 'city')
            ->leftJoin('d.land', 'land')
            ->addSelect('city')
            ->addSelect('land')
            //->orderBy('d.createdAt', 'DESC')
            ->orderBy('RAND()')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function search(array $criteria)
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.city', 'city')
            ->leftJoin('d.land', 'land');
            // ->leftJoin('d.labels', 'l'); // ajout de la jointure

        // Si des labels sont renseignés, on cherche les domaines qui ont un
        // nom, une description ou un label qui les contiennent.
        if (isset($criteria['keywords']) && !empty($criteria['keywords'])) {
            $qb->andWhere($qb->expr()->orX(
                    'd.name LIKE :keywords',
                    'd.description LIKE :keywords',
                    'l.name LIKE :keywords' // ajout de la condition sur les tags
                ))
                ->setParameter('keywords', '%' . $criteria['keywords'] . '%');
        }

        // Si une ville est renseignée, on cherche les domaines qui sont situés
        // dans cette ville.
        if (isset($criteria['city']) && !empty($criteria['city'])) {
            $cityName = strtolower($criteria['city']);
            $qb->andWhere('LOWER(city.name) = :city')
                ->setParameter('city', $cityName);
        }

        // Si une vallée est renseignée, on cherche les domaines qui sont dans
        // cette vallée.
        if (isset($criteria['land']) && !empty($criteria['land'])) {
            $landName = strtolower($criteria['land']);
            $qb->andWhere('LOWER(land.name) = :land')
                ->setParameter('land', $landName);
        }

        return $qb->getQuery()->getResult();
    }

    public function findByKeyword($keyword)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.name LIKE :keyword OR d.description LIKE :keyword')
            ->setParameter('keyword', '%' . $keyword . '%')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Domain[] Returns an array of Domain objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Domain
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
