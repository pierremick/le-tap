<?php

namespace App\Entity;

use App\Repository\DomainRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: DomainRepository::class)]
#[Vich\Uploadable]
class Domain
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 65, unique: true)]
    private ?string $name = null;

    #[ORM\Column(length: 65, unique: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[Vich\UploadableField(mapping: 'domain_thumbnail', fileNameProperty: 'thumbnailName')]
    private ?File $thumbnailFile = null;

    #[ORM\Column(nullable: true)]
    private ?string $thumbnailName = null;

    #[Vich\UploadableField(mapping: 'domain_featured', fileNameProperty: 'featuredName')]
    private ?File $featuredFile = null;

    #[ORM\Column(nullable: true)]
    private ?string $featuredName = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $address = null;

    #[ORM\ManyToOne(inversedBy: 'domains')]
    #[ORM\JoinColumn(nullable: false)]
    private ?City $city = null;

    #[ORM\Column(nullable: true, type: "float", scale: 7)]
    private ?float $lat = null;

    #[ORM\Column(nullable: true, type: "float", scale: 7)]
    private ?float $lng = null;

    #[ORM\ManyToOne(inversedBy: 'domains')]
    private ?Land $land = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 18, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $website = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $facebook = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $instagram = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkedin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pinterest = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $twitter = null;

    #[ORM\ManyToOne(inversedBy: 'domains')]
    private ?User $owner = null;

    #[ORM\Column(nullable: true)]
    private ?bool $eCommerce = null;

    #[ORM\Column(nullable: true)]
    private ?bool $vinBio = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sulfite = null;

    #[ORM\Column(nullable: true)]
    private ?bool $public = null;

    #[ORM\Column(nullable: true)]
    private ?bool $localEvent = null;

    #[ORM\Column(nullable: true)]
    private ?bool $balade = null;

    #[ORM\Column(nullable: true)]
    private ?bool $visite = null;

    #[ORM\Column(nullable: true)]
    private ?bool $piqueNique = null;

    #[ORM\Column(nullable: true)]
    private ?bool $restauration = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hebergement = null;

    #[ORM\Column(nullable: true)]
    private ?bool $location = null;

    #[ORM\Column(nullable: true)]
    private ?bool $groupe = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $cuvee = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $schedule = null;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setThumbnailFile(?File $thumbnailFile = null): void
    {
        $this->thumbnailFile = $thumbnailFile;

        if (null !== $thumbnailFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getThumbnailFile(): ?File
    {
        return $this->thumbnailFile;
    }

    public function setThumbnailName(?string $thumbnailName): void
    {
        $this->thumbnailName = $thumbnailName;
    }

    public function getThumbnailName(): ?string
    {
        return $this->thumbnailName;
    }

    public function setFeaturedFile(?File $featuredFile = null): void
    {
        $this->featuredFile = $featuredFile;

        if (null !== $featuredFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getFeaturedFile(): ?File
    {
        return $this->featuredFile;
    }

    public function setFeaturedName(?string $featuredName): void
    {
        $this->featuredName = $featuredName;
    }

    public function getFeaturedName(): ?string
    {
        return $this->featuredName;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(?float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getLand(): ?Land
    {
        return $this->land;
    }

    public function setLand(?Land $land): self
    {
        $this->land = $land;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getPinterest(): ?string
    {
        return $this->pinterest;
    }

    public function setPinterest(?string $pinterest): self
    {
        $this->pinterest = $pinterest;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function isECommerce(): ?bool
    {
        return $this->eCommerce;
    }

    public function setECommerce(?bool $eCommerce): self
    {
        $this->eCommerce = $eCommerce;

        return $this;
    }

    public function isVinBio(): ?bool
    {
        return $this->vinBio;
    }

    public function setVinBio(?bool $vinBio): self
    {
        $this->vinBio = $vinBio;

        return $this;
    }

    public function isSulfite(): ?bool
    {
        return $this->sulfite;
    }

    public function setSulfite(?bool $sulfite): self
    {
        $this->sulfite = $sulfite;

        return $this;
    }

    public function isPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function isLocalEvent(): ?bool
    {
        return $this->localEvent;
    }

    public function setLocalEvent(bool $localEvent): self
    {
        $this->localEvent = $localEvent;

        return $this;
    }

    public function isBalade(): ?bool
    {
        return $this->balade;
    }

    public function setBalade(?bool $balade): self
    {
        $this->balade = $balade;

        return $this;
    }

    public function isVisite(): ?bool
    {
        return $this->visite;
    }

    public function setVisite(?bool $visite): self
    {
        $this->visite = $visite;

        return $this;
    }

    public function isPiqueNique(): ?bool
    {
        return $this->piqueNique;
    }

    public function setPiqueNique(?bool $piqueNique): self
    {
        $this->piqueNique = $piqueNique;

        return $this;
    }

    public function isRestauration(): ?bool
    {
        return $this->restauration;
    }

    public function setRestauration(?bool $restauration): self
    {
        $this->restauration = $restauration;

        return $this;
    }

    public function isHebergement(): ?bool
    {
        return $this->hebergement;
    }

    public function setHebergement(?bool $hebergement): self
    {
        $this->hebergement = $hebergement;

        return $this;
    }

    public function isLocation(): ?bool
    {
        return $this->location;
    }

    public function setLocation(?bool $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function isGroupe(): ?bool
    {
        return $this->groupe;
    }

    public function setGroupe(?bool $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getCuvee(): ?string
    {
        return $this->cuvee;
    }

    public function setCuvee(?string $cuvee): self
    {
        $this->cuvee = $cuvee;

        return $this;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(?string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }
}
