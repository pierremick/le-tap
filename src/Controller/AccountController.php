<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\EditAccountType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('/mon-compte')]
class AccountController extends AbstractController
{
    #[Route('/', name: 'account')]
    public function account(): Response
    {
        return $this->render('account/index.html.twig', [
            'page_name' => 'Mon compte',
        ]);
    }

    #[Route('/infos-persos', name: 'edit_infos', methods: ['GET', 'POST'])]
    public function edit_infos(Request $request, UserRepository $userRepository): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(EditAccountType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTimeImmutable());
            $userRepository->save($user, true);

            return $this->redirectToRoute('account', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('account/edit_account.html.twig', [
            'user' => $user,
            'form' => $form,
            'page_name' => 'Éditer mes informations',
        ]);
    }

    #[Route('/mot-de-passe', name: 'change_password', methods: ['GET', 'POST'])]
    public function change_password(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $currentPassword = $data['current_password'];
            $newPassword = $data['new_password'];

            // Vérifie que le mot de passe actuel est correct
            if (!$userPasswordHasher->isPasswordValid($user, $currentPassword)) {
                $this->addFlash('danger', 'Le mot de passe actuel est incorrect.');

                return $this->redirectToRoute('change_password');
            }

            // Encode le nouveau mot de passe et le définit pour l'utilisateur
            $encodedPassword = $userPasswordHasher->hashPassword($user, $newPassword);
            $user->setPassword($encodedPassword);

            // Enregistre les modifications dans la base de données
            $user->setUpdatedAt(new \DateTimeImmutable());
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Le mot de passe a été modifié avec succès.');

            return $this->redirectToRoute('account');
        }

        return $this->renderForm('account/change_password.html.twig', [
            'form' => $form,
            'page_name' => 'Modifier mon mot de passe',
        ]);
    }
}
