<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Domain;
use App\Entity\Post;
use App\Form\ContactType;
use App\Form\DomainType;
use App\Form\PostType;
use App\Repository\ContactRepository;
use App\Repository\DomainRepository;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\UnicodeString;

#[Route('/backoffice')]
class BackofficeController extends AbstractController
{
    #[Route('/', name: 'backoffice')]
    public function backoffice(): Response
    {
        return $this->render('backoffice/index.html.twig', [
            'page_name' => 'Backoffice',
        ]);
    }

    #[Route('/contact', name: 'backoffice_contact_index', methods: ['GET'])]
    public function backoffice_contact_index(ContactRepository $contactRepository): Response
    {
        return $this->render('backoffice/contact/index.html.twig', [
            'contacts' => $contactRepository->findAll(),
            'page_name' => 'Contacts reçus',
        ]);
    }

    #[Route('/contact/{id}', name: 'backoffice_contact_show', methods: ['GET'])]
    public function backoffice_contact_show(Contact $contact): Response
    {
        return $this->render('backoffice/contact/show.html.twig', [
            'contact' => $contact,
            'page_name' => 'Contact',
        ]);
    }

    #[Route('/contact/{id}', name: 'backoffice_contact_delete', methods: ['POST'])]
    public function backoffice_contact_delete(Request $request, Contact $contact, ContactRepository $contactRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $contactRepository->remove($contact, true);
        }

        return $this->redirectToRoute('backoffice_contact_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/domain', name: 'backoffice_domain_index', methods: ['GET'])]
    public function backoffice_domain_index(DomainRepository $domainRepository): Response
    {
        return $this->render('backoffice/domain/index.html.twig', [
            'domains' => $domainRepository->findAllOptimized(),
            'page_name' => 'Domaines',
        ]);
    }

    #[Route('/domain/new', name: 'backoffice_domain_new', methods: ['GET', 'POST'])]
    public function backoffice_domain_new(Request $request, DomainRepository $domainRepository, SluggerInterface $slugger): Response
    {
        $domain = new Domain();
        $form = $this->createForm(DomainType::class, $domain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $domainName = (new UnicodeString($domain->getName()))->lower();
            $domain->setSlug($slugger->slug((string)$domainName));
            $domain->setCreatedAt(new \DateTimeImmutable());
            $domainRepository->save($domain, true);

            return $this->redirectToRoute('backoffice_domain_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backoffice/domain/new.html.twig', [
            'domain' => $domain,
            'form' => $form,
            'page_name' => 'Nouveau domaine',
        ]);
    }

    #[Route('/domain/{slug}', name: 'backoffice_domain_show', methods: ['GET'])]
    public function backoffice_domain_show(Domain $domain): Response
    {
        return $this->render('backoffice/domain/show.html.twig', [
            'domain' => $domain,
            'page_name' => $domain->getName(),
        ]);
    }

    #[Route('/domain/{slug}/edit', name: 'backoffice_domain_edit', methods: ['GET', 'POST'])]
    public function backoffice_domain_edit(Request $request, Domain $domain, DomainRepository $domainRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(DomainType::class, $domain);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $domainName = (new UnicodeString($domain->getName()))->lower();
            $domain->setSlug($slugger->slug((string)$domainName));
            $domain->setUpdatedAt(new \DateTimeImmutable());
            $domainRepository->save($domain, true);

            return $this->redirectToRoute('backoffice_domain_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backoffice/domain/edit.html.twig', [
            'domain' => $domain,
            'form' => $form,
            'page_name' => 'Éditer le domaine',
        ]);
    }

    #[Route('/domain/{slug}', name: 'backoffice_domain_delete', methods: ['POST'])]
    public function backoffice_domain_delete(Request $request, Domain $domain, DomainRepository $domainRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$domain->getId(), $request->request->get('_token'))) {
            $domainRepository->remove($domain, true);
        }

        return $this->redirectToRoute('backoffice_domain_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/post', name: 'backoffice_post_index', methods: ['GET'])]
    public function backoffice_post_index(PostRepository $postRepository): Response
    {
        return $this->render('backoffice/post/index.html.twig', [
            'posts' => $postRepository->findAll(),
            'page_name' => 'Articles',
        ]);
    }

    #[Route('/post/new', name: 'backoffice_post_new', methods: ['GET', 'POST'])]
    public function backoffice_post_new(Request $request, PostRepository $postRepository, SluggerInterface $slugger): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postName = (new UnicodeString($post->getTitle()))->lower();
            $post->setSlug($slugger->slug((string)$postName));
            $post->setCreatedAt(new \DateTimeImmutable());
            $postRepository->save($post, true);

            return $this->redirectToRoute('backoffice_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backoffice/post/new.html.twig', [
            'post' => $post,
            'form' => $form,
            'page_name' => 'Nouvel article',
        ]);
    }

    #[Route('/post/{id}', name: 'backoffice_post_show', methods: ['GET'])]
    public function backoffice_post_show(Post $post): Response
    {
        return $this->render('backoffice/post/show.html.twig', [
            'post' => $post,
            'page_name' => $post->getTitle(),
        ]);
    }

    #[Route('/post/{id}/edit', name: 'backoffice_post_edit', methods: ['GET', 'POST'])]
    public function backoffice_post_edit(Request $request, Post $post, PostRepository $postRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $postName = (new UnicodeString($post->getTitle()))->lower();
            $post->setSlug($slugger->slug((string)$postName));
            $post->setUpdatedAt(new \DateTimeImmutable());
            $postRepository->save($post, true);

            return $this->redirectToRoute('backoffice_post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backoffice/post/edit.html.twig', [
            'post' => $post,
            'form' => $form,
            'page_name' => 'Éditer l\'article',
        ]);
    }

    #[Route('/post/{id}', name: 'backoffice_post_delete', methods: ['POST'])]
    public function backoffice_post_delete(Request $request, Post $post, PostRepository $postRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $postRepository->remove($post, true);
        }

        return $this->redirectToRoute('backoffice_post_index', [], Response::HTTP_SEE_OTHER);
    }
}
