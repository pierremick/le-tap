<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Contact;
use App\Entity\Domain;
use App\Entity\Land;
use App\Entity\Post;
use App\Form\ContactType;
use App\Form\ContactServiceType;
use App\Form\SearchType;
use App\Repository\CategoryRepository;
use App\Repository\CityRepository;
use App\Repository\ContactRepository;
use App\Repository\DomainRepository;
use App\Repository\LandRepository;
use App\Repository\PostRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    #[Route('/', name: 'homepage', methods: ['GET', 'POST'])]
    public function homepage(Request $request, DomainRepository $domainRepository): Response
    {
        $latestDomains = $domainRepository->findByRandom(3);

        return $this->render('front/homepage.html.twig', [
            'page_name' => 'Le tap répertorie tous les vignerons',
            'page_description' => 'Le tap est un annuaire qui répertorie tous les Vignerons des Pyrénées-Orientales, qu\'ils soient de culture conventionnelle, bio ou Naturelle',
            'latestDomains' => $latestDomains,
        ]);
    }

    #[Route('/vigneron', name: 'domain_index', methods: ['GET', 'POST'])]
    public function domain_index(Request $request, DomainRepository $domainRepository, PaginatorInterface $paginator): Response
    {
        $form = $this->createForm(SearchType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $criteria = $form->getData();
            $domains = $domainRepository->search($criteria);
        } else {
            $domains = $domainRepository->findAllOptimized();
        }

        $pagination = $paginator->paginate(
            $domains,
            $request->query->getInt('page', 1),
            6 // Nombre d'éléments par page
        );

        return $this->render('front/domain_index.html.twig', [
            'form' => $form->createView(),
            'domains' => $pagination,
            'page_name' => 'Retrouvez tous les vignerons des Pyrénées-Orientales',
        ]);
    }

    #[Route('/vigneron/{slug}', name: 'domain_show', methods: ['GET'])]
    public function domain_show(Domain $domain): Response
    {
        return $this->render('front/domain_show.html.twig', [
            'domain' => $domain,
            'page_name' => $domain->getName(),
        ]);
    }

    #[Route('/le-mag', name: 'magazine', methods: ['GET'])]
    public function magazine(CategoryRepository $categoryRepository, PostRepository $postRepository): Response
    {
        $categories = $categoryRepository->findAll();
        $latestPostsByCategory = [];

        foreach ($categories as $category) {
            $latestPosts = $postRepository->findLatestByCategory($category, 3);
            $latestPostsByCategory[$category->getId()] = $latestPosts;
        }

        return $this->render('front/magazine.html.twig', [
            'categories' => $categories,
            'latestPostsByCategory' => $latestPostsByCategory,
            'page_name' => "Le magazine des vignerons",
        ]);
    }

    #[Route('/le-mag/{slug}', name: 'category', methods: ['GET'])]
    public function category(Category $category, PostRepository $postRepository): Response
    {
        $posts = $postRepository->findByCategorySlug($category->getSlug());

        return $this->render('front/category.html.twig', [
            'page_name' => $category->getName(),
            'category' => $category,
            'posts' => $posts,
        ]);
    }

    #[Route('/le-mag/{category}/{slug}', name: 'article', methods: ['GET'])]
    public function article(string $category, string $slug, Request $request, PostRepository $postRepository, CategoryRepository $categoryRepository): Response
    {
        $post = $postRepository->findOneBy([
            'slug' => $slug,
            'category' => $categoryRepository->findOneBy(['slug' => $category]),
        ]);

        if (!$post) {
            throw $this->createNotFoundException('Cet article n\'existe pas');
        }
        return $this->render('front/article.html.twig', [
            'post' => $post,
            'page_name' => $post->getTitle(),
        ]);
    }

    #[Route('/nous-contacter', name: 'contact', methods: ['GET', 'POST'])]
    public function contact(Request $request, ContactRepository $contactRepository): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setCreatedAt(new \DateTimeImmutable());
            $contactRepository->save($contact, true);

            return $this->redirectToRoute('thanks', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('front/contact.html.twig', [
            'contact' => $contact,
            'form' => $form,
            'page_name' => 'Contacter Le Tap, l\'annuaire de tous les vignerons',
        ]);
    }

    #[Route('/recherche', name: 'search_results', methods: ['GET'])]
    public function results(Request $request, PostRepository $postRepository, DomainRepository $domainRepository): Response
    {
        $keyword = $request->query->get('q'); // Récupérer le mot-clé de la requête GET

        // Effectuer la recherche dans les articles et les domaines
        $posts = $postRepository->findByKeyword($keyword);
        $domains = $domainRepository->findByKeyword($keyword);

        return $this->render('front/recherche.html.twig', [
            'keyword' => $keyword,
            'posts' => $posts,
            'domains' => $domains,
        ]);
    }

    #[Route('/nos-services-pour-les-vignerons', name: 'services', methods: ['GET', 'POST'])]
    public function services(Request $request, ContactRepository $contactRepository): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactServiceType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setCreatedAt(new \DateTimeImmutable());
            $contactRepository->save($contact, true);

            return $this->redirectToRoute('thanks', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('front/services.html.twig', [
            'contact' => $contact,
            'form' => $form,
            'page_name' => 'Nos services pour les vignerons',
            'page_description' => 'Le tap est un annuaire qui répertorie tous les Vignerons des Pyrénées-Orientales, qu\'ils soient de culture conventionnelle, bio ou Naturelle',
        ]);
    }

    #[Route('/merci', name: 'thanks')]
    public function thanks(): Response
    {
        return $this->render('front/thanks.html.twig', [
            'page_name' => 'Merci pour votre message !',
            'page_description' => 'Le tap est un annuaire qui répertorie tous les Vignerons des Pyrénées-Orientales, qu\'ils soient de culture conventionnelle, bio ou Naturelle',
        ]);
    }

    #[Route('/mentions-legales', name: 'legacy')]
    public function legacy(): Response
    {
        return $this->render('front/legacy.html.twig', [
            'page_name' => 'Mentions légales',
            'page_description' => 'Le tap est un annuaire qui répertorie tous les Vignerons des Pyrénées-Orientales, qu\'ils soient de culture conventionnelle, bio ou Naturelle',
        ]);
    }

    #[Route('/politique-de-confidentialite', name: 'privacy_policy')]
    public function privacy_policy(): Response
    {
        return $this->render('front/privacy_policy.html.twig', [
            'page_name' => 'Politique de confidentialité',
            'page_description' => 'Le tap est un annuaire qui répertorie tous les Vignerons des Pyrénées-Orientales, qu\'ils soient de culture conventionnelle, bio ou Naturelle',
        ]);
    }
}
