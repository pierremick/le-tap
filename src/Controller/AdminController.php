<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Land;
use App\Entity\Tag;
use App\Form\CategoryType;
use App\Form\CityType;
use App\Form\LandType;
use App\Form\TagType;
use App\Repository\CategoryRepository;
use App\Repository\CityRepository;
use App\Repository\LandRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\UnicodeString;

#[Route('/admin')]
class AdminController extends AbstractController
{
    #[Route('/', name: 'admin')]
    public function admin(): Response
    {
        return $this->render('admin/index.html.twig', [
            'page_name' => 'Admin',
        ]);
    }

    #[Route('/category', name: 'admin_category_index', methods: ['GET'])]
    public function admin_category_index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('admin/category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'page_name' => 'Catégories',
        ]);
    }

    #[Route('/category/new', name: 'admin_category_new', methods: ['GET', 'POST'])]
    public function admin_category_new(Request $request, CategoryRepository $categoryRepository, SluggerInterface $slugger): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryName = (new UnicodeString($category->getName()))->lower();
            $category->setSlug($slugger->slug((string)$categoryName));
            $category->setCreatedAt(new \DateTimeImmutable());
            $categoryRepository->save($category, true);

            return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/category/new.html.twig', [
            'category' => $category,
            'form' => $form,
            'page_name' => 'Nouvelle catégorie',
        ]);
    }

    #[Route('/category/{id}', name: 'admin_category_show', methods: ['GET'])]
    public function admin_category_show(Category $category): Response
    {
        return $this->render('admin/category/show.html.twig', [
            'category' => $category,
            'page_name' => 'Catégorie',
        ]);
    }

    #[Route('/category/{id}/edit', name: 'admin_category_edit', methods: ['GET', 'POST'])]
    public function admin_category_edit(Request $request, Category $category, CategoryRepository $categoryRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryName = (new UnicodeString($category->getName()))->lower();
            $category->setSlug($slugger->slug((string)$categoryName));
            $category->setUpdatedAt(new \DateTimeImmutable());
            $categoryRepository->save($category, true);

            return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/category/edit.html.twig', [
            'category' => $category,
            'form' => $form,
            'page_name' => 'Éditer la catégorie',
        ]);
    }

    #[Route('/category/{id}', name: 'admin_category_delete', methods: ['POST'])]
    public function admin_category_delete(Request $request, Category $category, CategoryRepository $categoryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $categoryRepository->remove($category, true);
        }

        return $this->redirectToRoute('admin_category_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/city', name: 'admin_city_index', methods: ['GET'])]
    public function admin_city_index(CityRepository $cityRepository): Response
    {
        return $this->render('admin/city/index.html.twig', [
            'cities' => $cityRepository->findAll(),
            'page_name' => 'Villes',
        ]);
    }

    #[Route('/city/new', name: 'admin_city_new', methods: ['GET', 'POST'])]
    public function admin_city_new(Request $request, CityRepository $cityRepository, SluggerInterface $slugger): Response
    {
        $city = new City();
        $form = $this->createForm(CityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cityName = (new UnicodeString($city->getName()))->lower();
            $city->setSlug($slugger->slug((string)$cityName));
            $city->setCreatedAt(new \DateTimeImmutable());
            $cityRepository->save($city, true);

            return $this->redirectToRoute('admin_city_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/city/new.html.twig', [
            'city' => $city,
            'form' => $form,
            'page_name' => 'Nouvelle ville',
        ]);
    }

    #[Route('/city/{id}', name: 'admin_city_show', methods: ['GET'])]
    public function admin_city_show(City $city): Response
    {
        return $this->render('admin/city/show.html.twig', [
            'city' => $city,
            'page_name' => $city->getName(),
        ]);
    }

    #[Route('/city/{id}/edit', name: 'admin_city_edit', methods: ['GET', 'POST'])]
    public function admin_city_edit(Request $request, City $city, CityRepository $cityRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(CityType::class, $city);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cityName = (new UnicodeString($city->getName()))->lower();
            $city->setSlug($slugger->slug((string)$cityName));
            $city->setUpdatedAt(new \DateTimeImmutable());
            $cityRepository->save($city, true);

            return $this->redirectToRoute('admin_city_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/city/edit.html.twig', [
            'city' => $city,
            'form' => $form,
            'page_name' => 'Éditer la ville',
        ]);
    }

    #[Route('/city/{id}', name: 'admin_city_delete', methods: ['POST'])]
    public function admin_city_delete(Request $request, City $city, CityRepository $cityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$city->getId(), $request->request->get('_token'))) {
            $cityRepository->remove($city, true);
        }

        return $this->redirectToRoute('admin_city_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/land', name: 'admin_land_index', methods: ['GET'])]
    public function admin_land_index(LandRepository $landRepository): Response
    {
        return $this->render('admin/land/index.html.twig', [
            'lands' => $landRepository->findAll(),
            'page_name' => 'Territoires',
        ]);
    }

    #[Route('/land/new', name: 'admin_land_new', methods: ['GET', 'POST'])]
    public function admin_land_new(Request $request, LandRepository $landRepository, SluggerInterface $slugger): Response
    {
        $land = new Land();
        $form = $this->createForm(LandType::class, $land);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $landName = (new UnicodeString($land->getName()))->lower();
            $land->setSlug($slugger->slug((string)$landName));
            $land->setCreatedAt(new \DateTimeImmutable());
            $landRepository->save($land, true);

            return $this->redirectToRoute('admin_land_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/land/new.html.twig', [
            'land' => $land,
            'form' => $form,
            'page_name' => 'Nouveau territoire',
        ]);
    }

    #[Route('/land/{id}', name: 'admin_land_show', methods: ['GET'])]
    public function admin_land_show(Land $land): Response
    {
        return $this->render('admin/land/show.html.twig', [
            'land' => $land,
            'page_name' => $land->getName(),
        ]);
    }

    #[Route('/land/{id}/edit', name: 'admin_land_edit', methods: ['GET', 'POST'])]
    public function admin_land_edit(Request $request, Land $land, LandRepository $landRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(LandType::class, $land);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $landName = (new UnicodeString($land->getName()))->lower();
            $land->setSlug($slugger->slug((string)$landName));
            $land->setUpdatedAt(new \DateTimeImmutable());
            $landRepository->save($land, true);

            return $this->redirectToRoute('admin_land_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/land/edit.html.twig', [
            'land' => $land,
            'form' => $form,
            'page_name' => 'Éditer le territoire',
        ]);
    }

    #[Route('/land/{id}', name: 'admin_land_delete', methods: ['POST'])]
    public function admin_land_delete(Request $request, Land $land, LandRepository $landRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$land->getId(), $request->request->get('_token'))) {
            $landRepository->remove($land, true);
        }

        return $this->redirectToRoute('admin_land_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/tag', name: 'admin_tag_index', methods: ['GET'])]
    public function admin_tag_index(TagRepository $tagRepository): Response
    {
        return $this->render('admin/tag/index.html.twig', [
            'tags' => $tagRepository->findAll(),
            'page_name' => '',
        ]);
    }

    #[Route('/tag/new', name: 'admin_tag_new', methods: ['GET', 'POST'])]
    public function admin_tag_new(Request $request, TagRepository $tagRepository, SluggerInterface $slugger): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tagName = (new UnicodeString($tag->getName()))->lower();
            $tag->setSlug($slugger->slug((string)$tagName));
            $tag->setCreatedAt(new \DateTimeImmutable());
            $tagRepository->save($tag, true);

            return $this->redirectToRoute('admin_tag_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/tag/new.html.twig', [
            'tag' => $tag,
            'form' => $form,
            'page_name' => '',
        ]);
    }

    #[Route('/tag/{id}', name: 'admin_tag_show', methods: ['GET'])]
    public function admin_tag_show(Tag $tag): Response
    {
        return $this->render('admin/tag/show.html.twig', [
            'tag' => $tag,
            'page_name' => '',
        ]);
    }

    #[Route('/tag/{id}/edit', name: 'admin_tag_edit', methods: ['GET', 'POST'])]
    public function admin_tag_edit(Request $request, Tag $tag, TagRepository $tagRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tagName = (new UnicodeString($tag->getName()))->lower();
            $tag->setSlug($slugger->slug((string)$tagName));
            $tag->setUpdatedAt(new \DateTimeImmutable());
            $tagRepository->save($tag, true);

            return $this->redirectToRoute('admin_tag_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/tag/edit.html.twig', [
            'tag' => $tag,
            'form' => $form,
            'page_name' => '',
        ]);
    }

    #[Route('/tag/{id}', name: 'admin_tag_delete', methods: ['POST'])]
    public function admin_tag_delete(Request $request, Tag $tag, TagRepository $tagRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tag->getId(), $request->request->get('_token'))) {
            $tagRepository->remove($tag, true);
        }

        return $this->redirectToRoute('admin_tag_index', [], Response::HTTP_SEE_OTHER);
    }
}
