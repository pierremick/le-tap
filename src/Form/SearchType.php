<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Land;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keywords', TextType::class, [
                'required' => false,
                'label' => 'Mots clés',
                'attr' => [
                    'placeholder' => 'Saisissez vos mots clés',
                ],
            ])
            ->add('city', EntityType::class, [
                'required' => false,
                'label' => 'Par ville',
                'class' => City::class,
                'choice_label' => 'name',
                'placeholder' => 'Affinez par ville',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->join('c.domains', 'd')
                        ->groupBy('c.id')
                        ->orderBy('c.name', 'ASC');
                },
            ])
            ->add('land', EntityType::class, [
                'required' => false,
                'label' => 'Par vallée',
                'class' => Land::class,
                'choice_label' => 'name',
                'placeholder' => 'Quelle vallée ?',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }
}
