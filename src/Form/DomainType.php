<?php

namespace App\Form;

use App\Entity\Domain;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
            ])
            ->add('land')
            ->add('owner')
            ->add('description', TextareaType::class, [
                'required' => true,
            ])
            ->add('schedule', CKEditorType::class, [
                'required' => false,
                'config' => [
                    'toolbar' => 'basic',
                ]
            ])
            ->add('address', TextType::class, [
                'required' => true,
            ])
            ->add('city')
            ->add('lat', NumberType::class, [
                'required' => false,
                'scale' => 7,
            ])
            ->add('lng', NumberType::class, [
                'required' => false,
                'scale' => 7,
            ])
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('phone', TelType::class, [
                'required' => false,
            ])
            ->add('website', UrlType::class, [
                'required' => false,
            ])
            ->add('facebook', UrlType::class, [
                'required' => false,
            ])
            ->add('instagram', UrlType::class, [
                'required' => false,
            ])
            ->add('linkedin', UrlType::class, [
                'required' => false,
            ])
            ->add('pinterest', UrlType::class, [
                'required' => false,
            ])
            ->add('twitter', UrlType::class, [
                'required' => false,
            ])
            //->add('features')
            //->add('labels')
            //->add('awards')
            ->add('cuvee', TextType::class, [
                'required' => false,
            ])
            ->add('eCommerce', CheckboxType::class, [
                'required' => false,
            ])
            ->add('vinBio', CheckboxType::class, [
                'required' => false,
            ])
            ->add('sulfite', CheckboxType::class, [
                'required' => false,
            ])
            ->add('public', CheckboxType::class, [
                'required' => false,
            ])
            ->add('localEvent', CheckboxType::class, [
                'required' => false,
            ])
            ->add('balade', CheckboxType::class, [
                'required' => false,
            ])
            ->add('visite', CheckboxType::class, [
                'required' => false,
            ])
            ->add('piqueNique', CheckboxType::class, [
                'required' => false,
            ])
            ->add('restauration', CheckboxType::class, [
                'required' => false,
            ])
            ->add('hebergement', CheckboxType::class, [
                'required' => false,
            ])
            ->add('location', CheckboxType::class, [
                'required' => false,
            ])
            ->add('groupe', CheckboxType::class, [
                'required' => false,
            ])
            ->add('thumbnailFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer',
                'download_label' => 'Télécharger',
                'download_uri' => true,
                'image_uri' => true,
                //'imagine_pattern' => '...',
                'asset_helper' => true,
            ])
            ->add('featuredFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer',
                'download_label' => 'Télécharger',
                'download_uri' => true,
                'image_uri' => true,
                //'imagine_pattern' => '...',
                'asset_helper' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Domain::class,
        ]);
    }
}
