<?php

namespace App\Form;

use App\Entity\Post;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
            ])
            ->add('category')
            ->add('description', TextareaType::class, [
                'required' => true,
            ])
            ->add('content', CKEditorType::class, [
                'required' => true,
                'config' => [
                    'format_tags' => 'p;h2;h3;h4;h5;h6',
                    'toolbar' => [
                        ['Undo', 'Redo'],
                        ['Format', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
                        ['NumberedList', 'BulletedList'],
                        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                        ['Link', 'Unlink'],
                        ['Anchor'],
                        ['HorizontalRule', 'SpecialChar'],
                        ['Table'],
                    ],
                ],
            ])
//            ->add('content', CKEditorType::class, [
//                'required' => true,
//                'config' => [
//                    'toolbar' => 'full',
//                ]
//            ])
            ->add('thumbnailFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true,
                'delete_label' => 'Supprimer',
                'download_label' => 'Télécharger',
                'download_uri' => true,
                'image_uri' => true,
                //'imagine_pattern' => '...',
                'asset_helper' => true,
            ])
            ->add('tags')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
