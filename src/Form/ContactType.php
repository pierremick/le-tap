<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints as Assert;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir votre prénom.',
                    ]),
                ],
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir votre nom.',
                    ]),
                ],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir votre adresse e-mail.',
                    ]),
                ],
            ])
            ->add('phone', TelType::class, [
                'required' => false,
            ])
            ->add('winemaker', CheckboxType::class, [
                'required' => false,
                'label' => 'Je suis vigneron',
            ])
            ->add('subject', ChoiceType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez choisir un objet pour votre demande.',
                    ]),
                ],
                'choices'  => [
                    'Choisissez' => null,
                    'Je veux enregistrer mon domaine dans l\'annuaire' => 'Enregistrer un domaine',
                    'Je veux modifier les informations de mon domaine' => 'Modifier un domaine',
                    'Je veux rejoindre la Team du Tap' => 'Rejoindre la Team',
                    'Autre demande' => 'Autre demande',
                ],
            ])
            ->add('message', TextareaType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir un message.',
                    ]),
                ],
            ])
            ->add('rgpd', CheckboxType::class, [
                'required' => true,
                'label' => 'Je suis d\'accord avec la politique de confidentialité du Tap',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter notre politique de confidentialité.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
