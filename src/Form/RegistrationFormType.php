<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Prénom',
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Nom',
            ])
            ->add('winemaker', CheckboxType::class, [
                'required' => false,
                'label' => 'Je suis vigneron',
            ])
            ->add('phone', TelType::class, [
                'required' => false,
                'label' => 'N° de téléphone',
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse e-mail',
            ])
//            ->add('password', RepeatedType::class, [
//                'type' => PasswordType::class,
//                'invalid_message' => 'Les deux champs mot de passe doivent correspondre.',
//                'options' => ['attr' => ['autocomplete' => 'new-password']],
//                'required' => true,
//                'first_options'  => ['label' => 'Mot de passe'],
//                'second_options' => ['label' => 'Répéter le mot de passe'],
//                'constraints' => [
//                    new NotBlank([
//                        'message' => 'Veuillez saisir un mot de passe',
//                    ]),
//                    new Length([
//                        'min' => 8,
//                        'minMessage' => 'Votre mot de passe doit comporter au moins {{ limit }} caractères',
//                        'max' => 4096,
//                    ]),
//                ],
//            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min' => 8,
                        'minMessage' => 'Le mot de passe doit comporter {{ limit }} caractères au minimum.'
                    ]),
                    new Assert\Regex([
                        'pattern' => '/[0-9]/',
                        'message' => 'Le mot de passe doit contenir au moins 1 chiffre.',
                    ]),
                    new Assert\Regex([
                        'pattern' => '/[A-Z]/',
                        'message' => 'Le mot de passe doit contenir au moins 1 lettre majuscule.',
                    ]),
                    new Assert\Regex([
                        'pattern' => '/[!@#$%^&*()\-_=+{};:,<.>]/',
                        'message' => 'Le mot de passe doit contenir au moins 1 caractère spécial.',
                    ]),
                ],
                'invalid_message' => 'Les deux champs de mot de passe doivent correspondre.',
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                    ],
                ],
                'required' => true,
                'first_options' => [
                    'label' => 'Mot de passe',
                ],
                'second_options' => [
                    'label' => 'Répéter le mot de passe',
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Je suis d\'accord avec la politique de confidentialité du Tap.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
