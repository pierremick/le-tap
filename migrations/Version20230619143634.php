<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230619143634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain_label DROP FOREIGN KEY FK_DDDC783E115F0EE5');
        $this->addSql('ALTER TABLE domain_label DROP FOREIGN KEY FK_DDDC783E33B92F39');
        $this->addSql('ALTER TABLE brewery_feature DROP FOREIGN KEY FK_F4E6D06860E4B879');
        $this->addSql('ALTER TABLE brewery_feature DROP FOREIGN KEY FK_F4E6D068D15C960');
        $this->addSql('ALTER TABLE domain_award DROP FOREIGN KEY FK_59200631115F0EE5');
        $this->addSql('ALTER TABLE domain_award DROP FOREIGN KEY FK_592006313D5282CF');
        $this->addSql('ALTER TABLE brewery_award DROP FOREIGN KEY FK_AC6C36413D5282CF');
        $this->addSql('ALTER TABLE brewery_award DROP FOREIGN KEY FK_AC6C3641D15C960');
        $this->addSql('ALTER TABLE domain_feature DROP FOREIGN KEY FK_FD006C7E60E4B879');
        $this->addSql('ALTER TABLE domain_feature DROP FOREIGN KEY FK_FD006C7E115F0EE5');
        $this->addSql('ALTER TABLE brewery_label DROP FOREIGN KEY FK_2890484E33B92F39');
        $this->addSql('ALTER TABLE brewery_label DROP FOREIGN KEY FK_2890484ED15C960');
        $this->addSql('DROP TABLE domain_label');
        $this->addSql('DROP TABLE brewery_feature');
        $this->addSql('DROP TABLE domain_award');
        $this->addSql('DROP TABLE feature');
        $this->addSql('DROP TABLE brewery_award');
        $this->addSql('DROP TABLE domain_feature');
        $this->addSql('DROP TABLE label');
        $this->addSql('DROP TABLE brewery_label');
        $this->addSql('DROP TABLE brewery');
        $this->addSql('DROP TABLE award');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE domain_label (domain_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_DDDC783E115F0EE5 (domain_id), INDEX IDX_DDDC783E33B92F39 (label_id), PRIMARY KEY(domain_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE brewery_feature (brewery_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_F4E6D06860E4B879 (feature_id), INDEX IDX_F4E6D068D15C960 (brewery_id), PRIMARY KEY(brewery_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE domain_award (domain_id INT NOT NULL, award_id INT NOT NULL, INDEX IDX_59200631115F0EE5 (domain_id), INDEX IDX_592006313D5282CF (award_id), PRIMARY KEY(domain_id, award_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE feature (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, icon VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_1FD775665E237E06 (name), UNIQUE INDEX UNIQ_1FD77566989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE brewery_award (brewery_id INT NOT NULL, award_id INT NOT NULL, INDEX IDX_AC6C3641D15C960 (brewery_id), INDEX IDX_AC6C36413D5282CF (award_id), PRIMARY KEY(brewery_id, award_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE domain_feature (domain_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_FD006C7E115F0EE5 (domain_id), INDEX IDX_FD006C7E60E4B879 (feature_id), PRIMARY KEY(domain_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE label (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, logo VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_EA750E85E237E06 (name), UNIQUE INDEX UNIQ_EA750E8989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE brewery_label (brewery_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_2890484ED15C960 (brewery_id), INDEX IDX_2890484E33B92F39 (label_id), PRIMARY KEY(brewery_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE brewery (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_1A5995475E237E06 (name), UNIQUE INDEX UNIQ_1A599547989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE award (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(65) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, logo VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8A5B2EE75E237E06 (name), UNIQUE INDEX UNIQ_8A5B2EE7989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE domain_label ADD CONSTRAINT FK_DDDC783E115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_label ADD CONSTRAINT FK_DDDC783E33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_feature ADD CONSTRAINT FK_F4E6D06860E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_feature ADD CONSTRAINT FK_F4E6D068D15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_award ADD CONSTRAINT FK_59200631115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_award ADD CONSTRAINT FK_592006313D5282CF FOREIGN KEY (award_id) REFERENCES award (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_award ADD CONSTRAINT FK_AC6C36413D5282CF FOREIGN KEY (award_id) REFERENCES award (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_award ADD CONSTRAINT FK_AC6C3641D15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_feature ADD CONSTRAINT FK_FD006C7E60E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_feature ADD CONSTRAINT FK_FD006C7E115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_label ADD CONSTRAINT FK_2890484E33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_label ADD CONSTRAINT FK_2890484ED15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
    }
}
