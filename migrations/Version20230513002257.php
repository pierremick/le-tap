<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230513002257 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE land (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) NOT NULL, slug VARCHAR(65) NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_A800D5D85E237E06 (name), UNIQUE INDEX UNIQ_A800D5D8989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE domain ADD land_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE domain ADD CONSTRAINT FK_A7A91E0B1994904A FOREIGN KEY (land_id) REFERENCES land (id)');
        $this->addSql('CREATE INDEX IDX_A7A91E0B1994904A ON domain (land_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain DROP FOREIGN KEY FK_A7A91E0B1994904A');
        $this->addSql('DROP TABLE land');
        $this->addSql('DROP INDEX IDX_A7A91E0B1994904A ON domain');
        $this->addSql('ALTER TABLE domain DROP land_id');
    }
}
