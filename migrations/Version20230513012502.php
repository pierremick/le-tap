<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230513012502 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain ADD e_commerce TINYINT(1) DEFAULT NULL, ADD vin_bio TINYINT(1) DEFAULT NULL, ADD sulfite TINYINT(1) DEFAULT NULL, ADD public TINYINT(1) DEFAULT NULL, ADD local_event TINYINT(1) DEFAULT NULL, ADD balade TINYINT(1) DEFAULT NULL, ADD visite TINYINT(1) DEFAULT NULL, ADD pique_nique TINYINT(1) DEFAULT NULL, ADD restauration TINYINT(1) DEFAULT NULL, ADD location TINYINT(1) DEFAULT NULL, ADD groupe TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain DROP e_commerce, DROP vin_bio, DROP sulfite, DROP public, DROP local_event, DROP balade, DROP visite, DROP pique_nique, DROP restauration, DROP location, DROP groupe');
    }
}
