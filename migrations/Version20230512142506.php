<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230512142506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE brewery_feature (brewery_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_F4E6D068D15C960 (brewery_id), INDEX IDX_F4E6D06860E4B879 (feature_id), PRIMARY KEY(brewery_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domain_feature (domain_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_FD006C7E115F0EE5 (domain_id), INDEX IDX_FD006C7E60E4B879 (feature_id), PRIMARY KEY(domain_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feature (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, icon VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_1FD775665E237E06 (name), UNIQUE INDEX UNIQ_1FD77566989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brewery_feature ADD CONSTRAINT FK_F4E6D068D15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_feature ADD CONSTRAINT FK_F4E6D06860E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_feature ADD CONSTRAINT FK_FD006C7E115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_feature ADD CONSTRAINT FK_FD006C7E60E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE brewery_feature DROP FOREIGN KEY FK_F4E6D068D15C960');
        $this->addSql('ALTER TABLE brewery_feature DROP FOREIGN KEY FK_F4E6D06860E4B879');
        $this->addSql('ALTER TABLE domain_feature DROP FOREIGN KEY FK_FD006C7E115F0EE5');
        $this->addSql('ALTER TABLE domain_feature DROP FOREIGN KEY FK_FD006C7E60E4B879');
        $this->addSql('DROP TABLE brewery_feature');
        $this->addSql('DROP TABLE domain_feature');
        $this->addSql('DROP TABLE feature');
    }
}
