<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230512143600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE brewery_label (brewery_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_2890484ED15C960 (brewery_id), INDEX IDX_2890484E33B92F39 (label_id), PRIMARY KEY(brewery_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domain_label (domain_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_DDDC783E115F0EE5 (domain_id), INDEX IDX_DDDC783E33B92F39 (label_id), PRIMARY KEY(domain_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE label (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) NOT NULL, slug VARCHAR(65) NOT NULL, description VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, creatde_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_EA750E85E237E06 (name), UNIQUE INDEX UNIQ_EA750E8989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brewery_label ADD CONSTRAINT FK_2890484ED15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_label ADD CONSTRAINT FK_2890484E33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_label ADD CONSTRAINT FK_DDDC783E115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_label ADD CONSTRAINT FK_DDDC783E33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE brewery_label DROP FOREIGN KEY FK_2890484ED15C960');
        $this->addSql('ALTER TABLE brewery_label DROP FOREIGN KEY FK_2890484E33B92F39');
        $this->addSql('ALTER TABLE domain_label DROP FOREIGN KEY FK_DDDC783E115F0EE5');
        $this->addSql('ALTER TABLE domain_label DROP FOREIGN KEY FK_DDDC783E33B92F39');
        $this->addSql('DROP TABLE brewery_label');
        $this->addSql('DROP TABLE domain_label');
        $this->addSql('DROP TABLE label');
    }
}
