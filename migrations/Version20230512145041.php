<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230512145041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE award (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(65) NOT NULL, slug VARCHAR(65) NOT NULL, description VARCHAR(255) DEFAULT NULL, logo VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8A5B2EE75E237E06 (name), UNIQUE INDEX UNIQ_8A5B2EE7989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE brewery_award (brewery_id INT NOT NULL, award_id INT NOT NULL, INDEX IDX_AC6C3641D15C960 (brewery_id), INDEX IDX_AC6C36413D5282CF (award_id), PRIMARY KEY(brewery_id, award_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE domain_award (domain_id INT NOT NULL, award_id INT NOT NULL, INDEX IDX_59200631115F0EE5 (domain_id), INDEX IDX_592006313D5282CF (award_id), PRIMARY KEY(domain_id, award_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brewery_award ADD CONSTRAINT FK_AC6C3641D15C960 FOREIGN KEY (brewery_id) REFERENCES brewery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brewery_award ADD CONSTRAINT FK_AC6C36413D5282CF FOREIGN KEY (award_id) REFERENCES award (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_award ADD CONSTRAINT FK_59200631115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE domain_award ADD CONSTRAINT FK_592006313D5282CF FOREIGN KEY (award_id) REFERENCES award (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE brewery_award DROP FOREIGN KEY FK_AC6C3641D15C960');
        $this->addSql('ALTER TABLE brewery_award DROP FOREIGN KEY FK_AC6C36413D5282CF');
        $this->addSql('ALTER TABLE domain_award DROP FOREIGN KEY FK_59200631115F0EE5');
        $this->addSql('ALTER TABLE domain_award DROP FOREIGN KEY FK_592006313D5282CF');
        $this->addSql('DROP TABLE award');
        $this->addSql('DROP TABLE brewery_award');
        $this->addSql('DROP TABLE domain_award');
    }
}
