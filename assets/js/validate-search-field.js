// Fonction de validation du champ de recherche
function validateSearchField() {
    var searchInput = document.getElementById('search-input');
    var searchValue = searchInput.value.trim();

    // Vérifie la longueur minimale requise
    if (searchValue.length < 3) {
        // Affiche un message d'erreur
        document.getElementById('search-error-message').innerText = 'La recherche doit contenir au moins 3 caractères.';
        return false;
    }

    // Vérifie les mots à ignorer
    var ignoredWords = [
        'ignorer1',
        'ignorer2',
        'ignorer3'
    ]; // Liste des mots à ignorer
    var words = searchValue.split(' ');

    for (var i = 0; i < words.length; i++) {
        var word = words[i].toLowerCase();

        if (ignoredWords.includes(word)) {
            // Affiche un message d'erreur
            document.getElementById('search-error-message').innerText = 'Certains mots sont à ignorer.';
            return false;
        }
    }

    // Validation réussie
    return true;
}

// Écoute l'événement de clic sur le bouton de recherche
var searchButton = document.getElementById('search-button');
searchButton.addEventListener('click', function (event) {
    // Effectue la validation du champ de recherche
    var isValid = validateSearchField();

    // Soumet le formulaire si la validation est réussie
    if (isValid) {
        var searchForm = document.getElementById('search-form');
        searchForm.submit();
    }
});

// Écoute l'événement de perte de focus du champ de recherche
var searchInput = document.getElementById('search-input');
searchInput.addEventListener('blur', function () {
    // Effectue la validation du champ de recherche
    validateSearchField();
});
